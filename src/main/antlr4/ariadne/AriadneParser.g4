parser grammar AriadneParser;

options { tokenVocab=AriadneLexer; }

document : query EOF ;

query
    : register
    | addproperty
    | addnode
    | addedge
    | find
    | removenode
    | removeedge
    | count
    | aggregation
    ;

register
    : REGISTERNODE registration #RegisterNode
    | REGISTEREDGE registration #RegisterEdge
    ;

registration
    : type=ID WITH (fields+=ID COMMA)+ AND fields+=ID
    | type=ID WITH fields+=ID (AND fields+=ID)*
    | type=ID
    ;

addnode
    : ADD specification #AddNode
    ;

addedge
    : ADD edge=specification FROM src=nodeReference TO dest=nodeReference #AddEdge
    ;

specification returns [String source, String specType]
    : type=ID WHERE fields+=ID IS values+=VALUE (AND fields+=ID IS values+=VALUE)*
    | OPEN_PAREN type   =ID WHERE fields+=ID IS values+=VALUE (AND fields+=ID IS values+=VALUE)* CLOSE_PAREN
    | type=ID WHERE (fields+=ID IS values+=VALUE COMMA)+ AND fields+=ID IS values+=VALUE
    | OPEN_PAREN type=ID WHERE (fields+=ID IS values+=VALUE COMMA)+ AND fields+=ID IS values+=VALUE CLOSE_PAREN
    | type=ID
    | OPEN_PAREN type=ID CLOSE_PAREN
    ;

find
    : WHAT src=nodeReference (edge=searchSpecification dest=nodeReference)?
    ;

count
    : HOWMANY src=nodeReference (edge=searchSpecification dest=nodeReference)?
    ;

searchSpecification returns [String source, List<String> queryVariables, String specType, List<String> compare]
    : type=idOpt WHERE comparisons+=comparison (AND comparisons+=comparison)*
    | OPEN_PAREN type=idOpt WHERE comparisons+=comparison (AND comparisons+=comparison)* CLOSE_PAREN
    | type=idOpt WHERE (comparisons+=comparison COMMA)+ AND comparisons+=comparison
    | OPEN_PAREN type=idOpt WHERE (comparisons+=comparison COMMA)+ AND comparisons+=comparison CLOSE_PAREN
    | type=idOpt
    | OPEN_PAREN type=idOpt CLOSE_PAREN
    ;

removenode
    : REMOVE node=nodeReference #RemoveNode
    ;

removeedge
    : REMOVE edge=searchSpecification FROM src=nodeReference TO dest=nodeReference #RemoveEdge
    ;

valueOpt
    : VALUE
    | UNKNOWN
    ;

nodeReference returns [String source, List<String> queryVariables, String specType, List<String> compare]
    : searchSpecification
    | specification
    ;

addproperty
    : ADDPROPERTY field=ID TO type=ID   #AddProperty
    ;

comparison returns [String field, String operator, String value, boolean queryVariable]
    : ID op=IS valueOpt
    | ID op=LESS_THAN VALUE
    | ID op=GREATER_THAN VALUE
    ;

idOpt
    : ID
    | UNKNOWN
    ;

aggregation
    : WHAT IS agg=aggSpec OF src=aggSearchSpec (edge=aggSearchSpec dest=aggSearchSpec)?
    ;

aggSpec
    : AGGREGATION ID
    ;

aggSearchSpec returns [String source, List<String> queryVariables, String specType, List<String> compare]
    : type=ID WHERE comparisons+=comparison (AND comparisons+=comparison)*
    | OPEN_PAREN type=ID WHERE comparisons+=comparison (AND comparisons+=comparison)* CLOSE_PAREN
    | type=ID WHERE (comparisons+=comparison COMMA)+ AND comparisons+=comparison
    | OPEN_PAREN type=ID WHERE (comparisons+=comparison COMMA)+ AND comparisons+=comparison CLOSE_PAREN
    | type=ID
    | OPEN_PAREN type=ID CLOSE_PAREN
    ;