package ariadne;

import org.antlr.v4.runtime.Token;

import java.io.*;
import java.util.*;

public class AriadneCompiler extends AriadneParserBaseListener {
    public Map<String, List<String>> nodes;
    public Map<String, List<String>> edges;
    public List<String> queries;
    private FileWriter fileWriter;
    private String settingsFileName;

    public AriadneCompiler(String settingsFileName) throws IOException {
        nodes = new TreeMap<>();
        edges = new TreeMap<>();
        queries = new ArrayList<>();

        this.settingsFileName = settingsFileName;
        File settingsFile = new File(settingsFileName);
        if (settingsFile.exists()) {
            Scanner settings = new Scanner(settingsFile);
            while (settings.hasNextLine()) {
                String[] tokens = settings.nextLine().split("=");
                String name = tokens[1];
                List<String> fields = new ArrayList<>();
                if (tokens.length != 2 && !tokens[2].isEmpty()) {
                    for (String field : tokens[2].split(",")) {
                        fields.add(field);
                    }
                }
                if (tokens[0].equals("node")) {
                    nodes.put(name, fields);
                } else if (tokens[0].equals("edge")) {
                    edges.put(name, fields);
                }
            }
        } else {
            PrintStream output = new PrintStream(settingsFile);
            output.print("");
            output.close();
        }
    }

    @Override
    public void enterDocument(AriadneParser.DocumentContext ctx) {
        try {
            fileWriter = new FileWriter(new File(settingsFileName), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void exitDocument(AriadneParser.DocumentContext ctx) {
        try {
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void enterRegisterEdge(AriadneParser.RegisterEdgeContext ctx) {
        String edgeType = ctx.registration().type.getText().toLowerCase();
        if (nodes.containsKey(edgeType)) {
            throw new IllegalArgumentException("An object type of that name already exists!");
        }
        if (edges.containsKey((edgeType))) {
            throw new IllegalArgumentException("A relationship type of that name already exists!");
        }
        List<String> fields = new ArrayList<>();
        for (Token t : ctx.registration().fields) {
            String fieldName = t.getText().toLowerCase();
            if (fields.contains(fieldName)) {
                throw new IllegalArgumentException("You cannot declare two fields of the same name: " + fieldName);
            }
            fields.add(fieldName);
        }
        System.out.println("Registered the relationship type " + edgeType + " with properties " + fields.toString() + "!");
        edges.put(edgeType, fields);
        try {
            fileWriter.write("edge=" + edgeType + "=" + String.join(",", fields) + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void enterRegisterNode(AriadneParser.RegisterNodeContext ctx) {
        String nodeType = ctx.registration().type.getText().toLowerCase();
        if (nodes.containsKey(nodeType)) {
            throw new IllegalArgumentException("An object type of that name already exists!");
        }
        if (edges.containsKey((nodeType))) {
            throw new IllegalArgumentException("A relationship type of that name already exists!");
        }
        List<String> fields = new ArrayList<>();
        for (Token t : ctx.registration().fields) {
            String fieldName = t.getText().toLowerCase();
            if (fields.contains(fieldName)) {
                throw new IllegalArgumentException("You cannot declare two fields of the same name: " + fieldName);
            }
            fields.add(fieldName);
        }
        System.out.println("Registered the object type " + nodeType + " with properties " + fields.toString() + "!");
        nodes.put(nodeType, fields);
        try {
            fileWriter.write("node=" + nodeType + "=" + String.join(",", fields) + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void enterAddNode(AriadneParser.AddNodeContext ctx) {
        checkSpecification(ctx.specification(), true);
    }

    @Override
    public void exitAddNode(AriadneParser.AddNodeContext ctx) {
        queries.add("CREATE (:" + ctx.specification().source + ")");
    }

    @Override
    public void enterAddEdge(AriadneParser.AddEdgeContext ctx) {
        checkSpecification(ctx.edge, false);
    }

    @Override
    public void exitAddEdge(AriadneParser.AddEdgeContext ctx) {
        checkNodeReference(ctx.src);
        checkNodeReference(ctx.dest);

        String srcName = ctx.src.specType.equals("?") ? "_object" : ctx.src.specType;
        String destName = ctx.dest.specType.equals("?") ? "_object" : ctx.dest.specType;
        if (srcName.equals(destName)) {
            srcName += "1";
            destName += "2";
        }
        String srcMatch = ctx.src.specType.equals("?") ? srcName : srcName + ":" + ctx.src.source;
        String destMatch = ctx.dest.specType.equals("?") ? destName : destName + ":" + ctx.dest.source;
        String query = "MATCH (" + srcMatch + "), (" + destMatch + ") ";
        List<String> finalConstraints = new ArrayList<>();
        addConstraints(finalConstraints, ctx.src.compare, srcName);
        addConstraints(finalConstraints, ctx.dest.compare, destName);
        if (!finalConstraints.isEmpty()) {
            query += " WHERE " + String.join(" AND ", finalConstraints);
        }
        query += " CREATE (" + srcName + ")-[:" + ctx.edge.source + "]->(" + destName + ")";
        queries.add(query);
    }

    @Override
    public void enterAddProperty(AriadneParser.AddPropertyContext ctx) {
        String type = ctx.type.getText().toLowerCase();
        String field = ctx.field.getText().toLowerCase();

        if (!nodes.containsKey(type) && !edges.containsKey(type)) {
            throw new IllegalArgumentException("There are no registered object or relationship types with the name " + type);
        }
        Map<String, List<String>> map = nodes.containsKey(type) ? nodes : edges;
        String nodeOrEdge = nodes.containsKey(type) ? "object" : "relationship";
        if (map.get(type).contains(field)) {
            throw new IllegalArgumentException("You cannot declare two fields of the same name: " + field);
        }
        System.out.println("Added the property " + field + " to the " + nodeOrEdge + " type " + type + "!");
        map.get(type).add(field);
        try {
            fileWriter.close();
            PrintStream output = new PrintStream(new File(settingsFileName));
            for (String nodeType : nodes.keySet()) {
                output.println("node=" + nodeType + "=" + String.join(",", nodes.get(nodeType)));
            }
            for (String edgeType : edges.keySet()) {
                output.println("edge=" + edgeType + "=" + String.join(",", edges.get(edgeType)));
            }
            output.close();
            fileWriter = new FileWriter(new File(settingsFileName), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void exitRemoveNode(AriadneParser.RemoveNodeContext ctx) {
        checkNodeReference(ctx.nodeReference());
        String srcName = ctx.node.specType.equals("?") ? "_object" : ctx.node.specType;
        String srcMatch = ctx.node.specType.equals("?") ? srcName : srcName + ":" + ctx.node.source;
        String query = "MATCH (" + srcMatch + ")";
        List<String> finalConstraints = new ArrayList<>();
        addConstraints(finalConstraints, ctx.node.compare, srcName);
        if (!finalConstraints.isEmpty()) {
            query += " WHERE " + String.join(" AND ", finalConstraints);
        }
        query += " DETACH DELETE " + srcName;
        queries.add(query);
    }

    @Override
    public void enterRemoveEdge(AriadneParser.RemoveEdgeContext ctx) {
        checkSearchSpecification(ctx.edge, false);
    }

    @Override
    public void exitRemoveEdge(AriadneParser.RemoveEdgeContext ctx) {
        checkNodeReference(ctx.src);
        checkNodeReference(ctx.dest);

        String srcName = ctx.src.specType.equals("?") ? "_object" : ctx.src.specType;
        String destName = ctx.dest.specType.equals("?") ? "_object" : ctx.dest.specType;
        if (srcName.equals(destName)) {
            srcName += "1";
            destName += "2";
        }
        String srcMatch = ctx.src.specType.equals("?") ? srcName : srcName + ":" + ctx.src.source;
        String destMatch = ctx.dest.specType.equals("?") ? destName : destName + ":" + ctx.dest.source;
        String edgeName = ctx.edge.specType.equals("?") ? "_relationship" : ctx.edge.specType;
        String edgeMatch = ctx.edge.specType.equals("?") ? edgeName : edgeName + ":" + ctx.edge.source;

        String query = "MATCH (" + srcMatch + ")-[" + edgeMatch + "]->(" + destMatch + ") ";
        List<String> finalConstraints = new ArrayList<>();
        addConstraints(finalConstraints, ctx.src.compare, srcName);
        addConstraints(finalConstraints, ctx.dest.compare, destName);
        if (!finalConstraints.isEmpty()) {
            query += " WHERE " + String.join(" AND ", finalConstraints);
        }
        query += " DELETE " + edgeName;
        queries.add(query);
    }

    @Override
    public void exitNodeReference(AriadneParser.NodeReferenceContext ctx) {
        if (ctx.specification() != null) {
            ctx.source = ctx.specification().source;
            ctx.specType = ctx.specification().specType;
        } else if (ctx.searchSpecification() != null) {
            ctx.source = ctx.searchSpecification().source;
            ctx.queryVariables = ctx.searchSpecification().queryVariables;
            ctx.specType = ctx.searchSpecification().specType;
            ctx.compare = ctx.searchSpecification().compare;
        }
    }

    // Sets source to be "id {field1: val1, field: val2, ... , fieldn: valn}"
    @Override
    public void exitSpecification(AriadneParser.SpecificationContext ctx) {
        ctx.source = ctx.type.getText().toLowerCase() + "{";
        List<String> arguments = new ArrayList<>();
        for (int i = 0; i < ctx.fields.size(); i++) {
            String fieldName = ctx.fields.get(i).getText().toLowerCase();
            String value = ctx.values.get(i).getText();
            arguments.add(fieldName + ":" + value);
        }
        ctx.source += String.join(", ", arguments) + "}";
        ctx.specType = ctx.type.getText().toLowerCase();
    }

    // Adds a query to the current queries
    @Override
    public void exitFind(AriadneParser.FindContext ctx) {
        checkNodeReference(ctx.src);
        String srcName = ctx.src.specType.equals("?") ? "_object" : ctx.src.specType;
        if (ctx.dest != null) {
            if (srcName.equals(ctx.dest.specType.equals("?") ? "_object" : ctx.dest.specType)) {
                srcName += "1";
            }
        }
        String srcMatch = ctx.src.specType.equals("?") ? srcName : srcName + ":" + ctx.src.source;
        String query = "MATCH (" + srcMatch + ")";
        List<String> finalConstraints = new ArrayList<>();
        List<String> variables = new ArrayList<>();
        addVariables(variables, ctx.src.queryVariables, true, ctx.src.specType, srcName);
        addConstraints(finalConstraints, ctx.src.compare, srcName);

        if (ctx.edge != null && ctx.dest != null) {
            checkSearchSpecification(ctx.edge, false);
            checkNodeReference(ctx.dest);

            String destName = ctx.dest.specType.equals("?") ? "_object" : ctx.dest.specType;
            if (srcName.equals(destName + "1")) {
                destName += "2";
            }
            String edgeName = ctx.edge.specType.equals("?") ? "_relationship" : ctx.edge.specType;
            String destMatch = ctx.dest.specType.equals("?") ? destName : destName + ":" + ctx.dest.source;
            String edgeMatch = ctx.edge.specType.equals("?") ? edgeName : edgeName + ":" + ctx.edge.source;

            query += "-[" + edgeMatch + "]->(" + destMatch + ")";

            addConstraints(finalConstraints, ctx.dest.compare, destName);
            addConstraints(finalConstraints, ctx.edge.compare, edgeName);
            addVariables(variables, ctx.dest.queryVariables, true, ctx.dest.specType, destName);
            addVariables(variables, ctx.edge.queryVariables, false, ctx.edge.specType, edgeName);
        }
        if (!finalConstraints.isEmpty()) {
            query += " WHERE " + String.join(" AND ", finalConstraints);
        }
        query += " RETURN " + String.join(", ", variables);
        queries.add(query);
    }

    @Override
    public void exitCount(AriadneParser.CountContext ctx) {
        checkNodeReference(ctx.src);
        String srcName = ctx.src.specType.equals("?") ? "_object" : ctx.src.specType;
        if (ctx.dest != null) {
            if (srcName.equals(ctx.dest.specType.equals("?") ? "_object" : ctx.dest.specType)) {
                srcName += "1";
            }
        }
        String srcMatch = ctx.src.specType.equals("?") ? srcName : srcName + ":" + ctx.src.source;
        String query = "MATCH (" + srcMatch + ")";
        List<String> finalConstraints = new ArrayList<>();
        List<String> variables = new ArrayList<>();
        addVariables(variables, ctx.src.queryVariables, true, ctx.src.specType, srcName);
        addConstraints(finalConstraints, ctx.src.compare, srcName);

        if (ctx.edge != null && ctx.dest != null) {
            checkSearchSpecification(ctx.edge, false);
            checkNodeReference(ctx.dest);

            String destName = ctx.dest.specType.equals("?") ? "_object" : ctx.dest.specType;
            if (srcName.equals(destName + "1")) {
                destName += "2";
            }
            String edgeName = ctx.edge.specType.equals("?") ? "_relationship" : ctx.edge.specType;
            String destMatch = ctx.dest.specType.equals("?") ? destName : destName + ":" + ctx.dest.source;
            String edgeMatch = ctx.edge.specType.equals("?") ? edgeName : edgeName + ":" + ctx.edge.source;

            query += "-[" + edgeMatch + "]->(" + destMatch + ")";

            addConstraints(finalConstraints, ctx.dest.compare, destName);
            addConstraints(finalConstraints, ctx.edge.compare, edgeName);
            addVariables(variables, ctx.dest.queryVariables, true, ctx.dest.specType, destName);
            addVariables(variables, ctx.edge.queryVariables, false, ctx.edge.specType, edgeName);
        }
        if (!finalConstraints.isEmpty()) {
            query += " WHERE " + String.join(" AND ", finalConstraints);
        }
        query += " RETURN count(*) as count";
        queries.add(query);
    }

    private void addConstraints(List<String> finalConstraints, List<String> comparisons, String name) {
        for (String comparison : comparisons) {
            finalConstraints.add(name + "." + comparison);
        }
    }

    private void addVariables(List<String> finalVariables, List<String> queryVariables, boolean checkNode, String type, String name) {
        if (name.startsWith("_object")) {
            finalVariables.add("labels(" + name + ")");
            finalVariables.add(name);
        } else if (name.equals("_relationship")) {
            finalVariables.add(name);
        } else if (queryVariables.isEmpty()) { // If they don't specify any queryVariables, add all their fields as a query variable
            if (checkNode) {
                for (String field : nodes.get((type))) {
                    finalVariables.add(name + "." + field);
                }
            } else {
                for (String field : edges.get((type))) {
                    finalVariables.add(name + "." + field);
                }
            }
        } else { // Otherwise, just add the specified query variables
            for (String variable : queryVariables) {
                finalVariables.add(name + "." + variable);
            }
        }
    }

    // Sets .source to be "id {field1: val1, field: val2, ... , fieldn: valn}"
    // and .queryVariables to be a list of all field names to be queried on
    @Override
    public void exitSearchSpecification(AriadneParser.SearchSpecificationContext ctx) {
        ctx.specType = ctx.type.getText().toLowerCase();
        ctx.queryVariables = new ArrayList<>();
        ctx.compare = new ArrayList<>();
        ctx.source = ctx.type.getText().toLowerCase() + "{";
        for (int i = 0; i < ctx.comparisons.size(); i++) {
            AriadneParser.ComparisonContext comparison = ctx.comparisons.get(i);
            String fieldName = comparison.field;
            String value = comparison.value;
            String operator = comparison.operator;
            if (value.equals("?")) {
                ctx.queryVariables.add(fieldName);
            } else {
                ctx.compare.add(fieldName + operator + value);
            }
        }
        ctx.source += "}";
    }

    @Override
    public void exitComparison(AriadneParser.ComparisonContext ctx) {
        ctx.field = ctx.ID().getText().toLowerCase();
        ctx.operator = ctx.op.getText().toLowerCase();
        if (ctx.operator.equals("is")) {
            ctx.operator = "=";
        }
        ctx.value = ctx.valueOpt() != null ? ctx.valueOpt().getText() : ctx.VALUE().getText();
    }

    @Override
    public void exitAggregation(AriadneParser.AggregationContext ctx) {
        checkAggSearchSpec(ctx.src, true);
        String srcName = ctx.src.specType;
        if (ctx.dest != null) {
            if (srcName.equals(ctx.dest.specType)) {
                srcName += "1";
            }
        }
        String srcMatch = srcName + ":" + ctx.src.source;
        String query = "MATCH (" + srcMatch + ")";
        List<String> finalConstraints = new ArrayList<>();
        List<String> variables = new ArrayList<>();
        addConstraints(finalConstraints, ctx.src.compare, srcName);
        addAggregations(variables, finalConstraints, ctx.agg, srcName, ctx.src.specType);

        if (ctx.edge != null && ctx.dest != null) {
            checkAggSearchSpec(ctx.edge, false);
            checkAggSearchSpec(ctx.dest , true);

            String destName = ctx.dest.specType;
            if (srcName.equals(destName + "1")) {
                destName += "2";
            }
            String edgeName = ctx.edge.specType;
            String destMatch = destName + ":" + ctx.dest.source;
            String edgeMatch = edgeName + ":" + ctx.edge.source;

            query += "-[" + edgeMatch + "]->(" + destMatch + ")";

            addConstraints(finalConstraints, ctx.dest.compare, destName);
            addConstraints(finalConstraints, ctx.edge.compare, edgeName);
            addAggregations(variables, finalConstraints, ctx.agg, destName, ctx.dest.specType);
            addAggregations(variables, finalConstraints, ctx.agg, edgeName, ctx.edge.specType);

        }
        if (!finalConstraints.isEmpty()) {
            query += " WHERE " + String.join(" AND ", finalConstraints);
        }
        if (!variables.isEmpty()) {
            query += " RETURN " + String.join(", ", variables);
            queries.add(query);
        } else {
            throw new IllegalArgumentException("None of the matched objects or relationships have that field:" + ctx.agg.ID().getText().toLowerCase());
        }

    }

    private void addAggregations(List<String> variables, List<String> finalConstraints,
                                 AriadneParser.AggSpecContext agg, String name, String specType) {
        String fieldName = agg.ID().getText().toLowerCase();
        if ((nodes.containsKey(specType) && nodes.get(specType).contains(fieldName)) ||
                    (edges.containsKey(specType) && edges.get(specType).contains(fieldName))) {
                finalConstraints.add(name + "." + fieldName + " <> toString(" + name + "." + fieldName + ")");
                String aggregation = agg.AGGREGATION().getText().toUpperCase();
                aggregation = aggregation.equals("AVERAGE") ? "AVG" : aggregation;
                variables.add(aggregation + "(" + name + "." + fieldName + ")");
        }
    }

    @Override
    public void exitAggSearchSpec(AriadneParser.AggSearchSpecContext ctx) {
        ctx.specType = ctx.type.getText().toLowerCase();
        ctx.queryVariables = new ArrayList<>();
        ctx.compare = new ArrayList<>();
        ctx.source = ctx.type.getText().toLowerCase() + "{";
        for (int i = 0; i < ctx.comparisons.size(); i++) {
            AriadneParser.ComparisonContext comparison = ctx.comparisons.get(i);
            String fieldName = comparison.field;
            String value = comparison.value;
            String operator = comparison.operator;
            if (value.equals("?")) {
                ctx.queryVariables.add(fieldName);
            } else {
                ctx.compare.add(fieldName + operator + value);
            }
        }
        ctx.source += "}";
    }



    // Ensures that the given NodeReference is a valid specification, searchSpecification, or variable reference
    private void checkNodeReference(AriadneParser.NodeReferenceContext ctx) {
        if (ctx.specification() != null) {
            checkSpecification(ctx.specification(), true);
        } else {
            checkSearchSpecification(ctx.searchSpecification(), true);
        }
    }


    // Ensure that the given specification is valid.
    // This means that the type of the specification has been registered
    // This also means that all fields in the specification were defined when the type has been registered
    private void checkSpecification(AriadneParser.SpecificationContext ctx, boolean checkNode) {
        Map<String, List<String>> map = checkNode ? nodes : edges;
        String type = checkNode ? "Object" : "Relationship";
        String name = ctx.type.getText().toLowerCase();
        if (name.equals("?")) {
            return;
        }
        if (!map.containsKey(name)) {
            throw new IllegalArgumentException("You have not registered an " + type + " of name: " + name + "\n" +
                    "Here are a list of registered " + type +"s: " + map.keySet().toString());
        }
        for (Token t : ctx.fields) {
            String fieldName = t.getText().toLowerCase();
            if (!map.get(name).contains(fieldName)) {
                throw new IllegalArgumentException(type + "s of type " + name + " do not have the property: "
                        + fieldName + "\n They have the following properties " + map.get(name));
            }
        }
    }


    // Ensure that the given specification is valid.
    // This means that the type of the specification has been registered
    // This also means that all fields in the specification were defined when the type has been registered
    private void checkSearchSpecification(AriadneParser.SearchSpecificationContext ctx, boolean checkNode) {
        Map<String, List<String>> map = checkNode ? nodes : edges;
        String type = checkNode ? "Object" : "Relationship";
        String name = ctx.type.getText().toLowerCase();
        if (name.equals("?")) {
            return;
        }
        if (!map.containsKey(name)) {
            throw new IllegalArgumentException("You have not registered an " + type + " of name: " + name + "\n" +
                    "Here are a list of registered " + type +"s: " + map.keySet().toString());
        }
        for (AriadneParser.ComparisonContext cc : ctx.comparisons) {
            String fieldName = cc.field;
            if (!map.get(name).contains(fieldName)) {
                throw new IllegalArgumentException(type + "s of type " + name + " do not have the property: "
                        + fieldName + "\n They have the following properties " + map.get(name));
            }
        }
    }

    // Ensure that the given specification is valid.
    // This means that the type of the specification has been registered
    // This also means that all fields in the specification were defined when the type has been registered
    private void checkAggSearchSpec(AriadneParser.AggSearchSpecContext ctx, boolean checkNode) {
        Map<String, List<String>> map = checkNode ? nodes : edges;
        String type = checkNode ? "Object" : "Relationship";
        String name = ctx.type.getText().toLowerCase();
        if (name.equals("?")) {
            return;
        }
        if (!map.containsKey(name)) {
            throw new IllegalArgumentException("You have not registered an " + type + " of name: " + name + "\n" +
                    "Here are a list of registered " + type +"s: " + map.keySet().toString());
        }
        for (AriadneParser.ComparisonContext cc : ctx.comparisons) {
            String fieldName = cc.field;
            if (!map.get(name).contains(fieldName)) {
                throw new IllegalArgumentException(type + "s of type " + name + " do not have the property: "
                        + fieldName + "\n They have the following properties " + map.get(name));
            }
        }
    }
}