package ariadne;


import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.text.similarity.LongestCommonSubsequence;

import java.io.IOException;
import java.sql.*;
import java.util.*;

public class AriadneShell {
    private static final String SETTINGS_FILE = ".settings.ariadne";

    private static final String[] QUERIES = {"RegisterObject", "RegisterRelationship",
            "Add", "Remove", "What", "AddProperty", "HowMany",
            "view", "objects", "relationships", "help", "quit"};

    private static final Map<String, String> QUERY_HELP = new LinkedHashMap<>();

    private static final String PROMPT = "Type a query, or:\n" +
            "    \"view\" to list all objects stored\n" +
            "    \"objects\" to list all registered object types\n" +
            "    \"relationships\" to list all registered relationship types\n" +
            "    \"help\" to get help\n" +
            "    \"quit\" to quit";

    public static void main(String[] args) {
        try {
            Scanner console = new Scanner(System.in);
            AriadneCompiler listener = new AriadneCompiler(SETTINGS_FILE);

            // connecting to database
            String password = "password";
            try (Connection con = DriverManager.getConnection("jdbc:neo4j:bolt://localhost", "neo4j", password)) {
                // start up shell
                initializeQueryHelp();
                System.out.println(PROMPT);
                while (true) {
                    System.out.print("-> ");
                    String input = console.nextLine().trim();
                    if (input.equalsIgnoreCase("view")) {
                        listAllNodes(con);
                    } else if (input.equalsIgnoreCase("objects")){
                        for (String object : listener.nodes.keySet()) {
                            System.out.println(object + listener.nodes.get(object));
                        }
                    } else if (input.equalsIgnoreCase("relationships")) {
                        for (String relationship : listener.edges.keySet()) {
                            System.out.println(relationship + listener.edges.get(relationship));
                        }
                    } else if (input.equalsIgnoreCase("help")) {
                        printHelp();
                    } else if (input.equalsIgnoreCase("quit")) {
                        System.out.println("Goodbye :)");
                        break;
                    } else {
                        try {
                            processQuery(input, con, listener);
                        } catch (IllegalArgumentException e) {
                            System.out.println("ERROR: " + e.getMessage());
                        } catch (ParseCancellationException e) {
                            Scanner lineScan = new Scanner(input);
                            if (lineScan.hasNext()) {
                                Scanner inputScan = new Scanner(input);
                                String firstToken = inputScan.next().toLowerCase();
                                if (inputScan.hasNext()) {
                                    firstToken += inputScan.next().toLowerCase();
                                }
                                LongestCommonSubsequence distanceCalc = new LongestCommonSubsequence();
                                String closestMatch = null;
                                double bestDistance = -1;
                                for (String query : QUERIES) {
                                    double distance = distanceCalc.longestCommonSubsequence(firstToken, query.toLowerCase()).length() * 1.0 / query.length();
                                    if (distance > bestDistance) {
                                        closestMatch = query;
                                        bestDistance = distance;
                                    }
                                }
                                String errorMessage = e.getMessage();
                                Scanner messageScan = new Scanner(errorMessage);
                                messageScan.next();
                                messageScan.next();
                                errorMessage = messageScan.next();

                                System.out.println("There was a syntax error at the word " + errorMessage);
                                System.out.println("Did you mean to use the query '" + closestMatch + "'?");
                                if (Character.isUpperCase(closestMatch.charAt(0))) {
                                    System.out.println("Here is an example of that query:");
                                    System.out.println(QUERY_HELP.get(closestMatch));
                                }
                                System.out.println();
                            }
                        }
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void listAllNodes(Connection con) {
        try {
            String cypherQuery = "MATCH (n) RETURN n";
            try (PreparedStatement stmt = con.prepareStatement(cypherQuery)) {
                try (ResultSet rs = stmt.executeQuery()) {
                    while (rs.next()) {
                        printPretty(rs.getObject(1), true);
                    }
                    System.out.println();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void printHelp() {
        System.out.println();
        System.out.println("Welcome to the Ariadne database. Information is stored as objects and\n" +
                "relationships. Here are examples of the possible commands you can run to\n" +
                "add new information or view previously stored information:\n" +
                "\n" +
                "    RegisterObject MOVIE with TITLE and RELEASED\n" +
                "    RegisterRelationship STARRED_IN with ROLE\n" +
                "    RegisterObject PERSON with NAME and BIRTH_YEAR\n" +
                "    Add MOVIE where TITLE is 'The Matrix' and RELEASED is 1999\n" +
                "    Add PERSON where NAME is 'Keanu Reeves' and BIRTH_YEAR is 1964\n" +
                "    Add STARRED_IN where ROLE is 'Neo' from PERSON where NAME is 'Keanu Reeves' to MOVIE where TITLE is 'The Matrix' and RELEASED is 1999\n" +
                "    What ACTOR STARRED_IN MOVIE where TITLE is 'The Matrix'\n" +
                "    HowMany ACTOR STARRED_IN MOVIE where TITLE is 'The Matrix'\n" +
                "    What is max BIRTH_YEAR of ACTOR\n" +
                "    Remove MOVIE where RELEASED is 1999\n" +
                "    Remove STARRED_IN from PERSON where BIRTH_YEAR is 1964 to MOVIE where TITLE is 'The Matrix' and RELEASED is 1999\n" +
                "\n" +
                "For more information, visit this link: https://docs.google.com/document/d/1gotkiR2daIcH3dBg9QrXkTLBGbharlOsgJpSouqTIxU/edit");
        System.out.println();
        System.out.println(PROMPT);
    }

    private static void processQuery(String ariadneQuery, Connection con, AriadneCompiler listener) throws SQLException {
        AriadneLexer ariadneLexer = new AriadneLexer(new CaseChangingCharStream(CharStreams.fromString(ariadneQuery), false));
        ariadneLexer.removeErrorListeners();
        ariadneLexer.addErrorListener(AriadneErrorListener.INSTANCE);

        CommonTokenStream tokens = new CommonTokenStream(ariadneLexer);
        AriadneParser parser = new AriadneParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(AriadneErrorListener.INSTANCE);

        parser.setBuildParseTree(true);
        ParseTree tree = parser.document();
        org.antlr.v4.runtime.tree.ParseTreeWalker.DEFAULT.walk(listener, tree);


        // query the database (if applicable)
        List<String> cypherQueries = listener.queries;
        if (cypherQueries.isEmpty() || ariadneQuery.startsWith("Register") || ariadneQuery.startsWith("AddProperty")) {
            return;
        }
        String cypherQuery = cypherQueries.get(cypherQueries.size() - 1);
            try (PreparedStatement stmt = con.prepareStatement(cypherQuery)) {
            try (ResultSet rs = stmt.executeQuery()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int numRows = 0;
                boolean allNull = true;
                while(rs.next()) {
                    numRows++;
                    if (rs.getString(1) != null) {
                        allNull = false;
                    }
                }
                if (numRows == 1 && allNull && rsmd.getColumnCount() == 1) {
                    System.out.println("No results");
                    return;
                }
                ResultSet rs2 = stmt.executeQuery();
                rsmd = rs2.getMetaData();
                System.out.println("---------------------");
                while (rs2.next()) {
                    boolean hasFields = false;
                    for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                        if (rsmd.getColumnName(i).startsWith("_")) {
                            String columnName = rsmd.getColumnName(i);
                            Object result = rs2.getObject(i);
                            System.out.println("_" + columnName + "__");
                            printPretty(result, columnName.startsWith("_object"));
                            hasFields = true;
                        } else if (rs2.getString(i) != null) {
                            System.out.println(rsmd.getColumnName(i) + "=" + rs2.getString(i));
                            hasFields = true;
                        }
                    }
                    if (!hasFields && rsmd.getColumnCount() >= 1) {
                        String type = rsmd.getColumnName(1).split("\\.")[0];
                        List<String> fields = new ArrayList<>();
                        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                            fields.add(rsmd.getColumnName(i).split("\\.")[1]);
                        }
                        System.out.println(type + " with no " + String.join(" or ", fields));
                    }
                    System.out.println("---------------------");
                }
                System.out.println();
            }
        }
    }

    /**
     *
     * @param obj JSON-like format returned by neo4j
     * @param isNode true if printing a node/object, false if printing an edge/relationship
     */
    private static void printPretty(Object obj, boolean isNode) {
        Map<String, Object> map = (Map<String, Object>) obj;
        String nodeType = isNode
                ? ((List<String>) map.get("_labels")).get(0)
                : map.get("_type").toString();
        String where = map.keySet().size() > 4 || isNode && map.keySet().size() > 2
                ? " where "
                : "";
        System.out.print(nodeType + where);
        if (where.isEmpty()) {
            System.out.println();
            return;
        }
        List<String> fields = new ArrayList<>();
        for (String left : map.keySet()) {
            if (!left.startsWith("_")) {
                Object value = map.get(left);
                String right = value instanceof String ? "'" + value + "'" : value.toString();
                fields.add(left + " is " + right);
            }
        }
        System.out.println(String.join(" and ", fields));
    }

    private static void initializeQueryHelp() {
        QUERY_HELP.put("RegisterObject", "RegisterObject MOVIE with TITLE and RELEASED");
        QUERY_HELP.put("RegisterRelationship", "RegisterRelationship STARRED_IN with ROLE");
        QUERY_HELP.put("Add", "Add MOVIE where TITLE is 'The Matrix' and RELEASED is 1999\n" +
                "Add STARRED_IN where ROLE is 'Neo' from PERSON where NAME is 'Keanu Reeves' " +
                "to MOVIE where TITLE is 'The Matrix' and RELEASED is 1999");
        QUERY_HELP.put("Remove", "Remove MOVIE where RELEASED is 1999\n" +
                "Remove STARRED_IN from PERSON where BIRTH_YEAR is 1964 to MOVIE where" +
                " TITLE is 'The Matrix' and RELEASED is 1999");
        QUERY_HELP.put("What", "What ACTOR STARRED_IN MOVIE where TITLE is 'The Matrix'");
        QUERY_HELP.put("AddProperty", "AddProperty RATING to MOVIE");
        QUERY_HELP.put("HowMany", "HowMany ACTOR STARRED_IN MOVIE where TITLE is 'The Matrix'");
    }
}