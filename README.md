# Ariadne

pre-proposal: https://docs.google.com/document/d/1NmvQgbMwu9934z1FbhyFD7Ndxoy8E8Og86gequUOHm0/edit?usp=sharing

proposal: https://docs.google.com/document/d/1TucPTMHC-TRnZ7hAatp_nC-19poSzLFj4rKiH2v8aSQ/edit?usp=sharing

design document: https://docs.google.com/document/d/1sRgCjvuU9Zpevt1kH5jBNpvtgtzowsx2Bmo6G0mBCX4/edit?usp=sharing

final report: https://docs.google.com/document/d/1kuM0E0dQM3IQvUNouNo32oWsn9uAK0OHTf2vYyCT89A/edit?usp=sharing

tutorial + worked examples:https://docs.google.com/document/d/1gotkiR2daIcH3dBg9QrXkTLBGbharlOsgJpSouqTIxU/edit?usp=sharing

screencast demo: https://vimeo.com/341939052

poster: https://drive.google.com/file/d/12MPRcuX5PZab7XKfhV2z1SOokyJpJOE1/view?usp=sharing
